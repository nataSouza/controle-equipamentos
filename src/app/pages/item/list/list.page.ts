import { Component, OnInit, ViewChild } from '@angular/core';
import Item from 'src/app/entities/Item';
import { NavController } from '@ionic/angular';
import ItemModel from 'src/app/models/ItemModel';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  	itens : Array<Item>;
	itensDisponiveis : Array<Item>;


	public callback: Function;

	constructor(private navCtrl: NavController) {
	}

	ngOnInit(){
		console.log('ngOnInit')
		this.callback = this.loadItens.bind(this);
	}

	filterItensDisponiveis = item => item.qtd > 0;

	go(route: string) {
		console.log('Navegando entre telas...');
		this.navCtrl.navigateRoot([route]);
	}

	ionViewWillEnter() {
		console.log('ionViewWillEnter')
		this.loadItens();
	}



	loadItens(){
		const itens = ItemModel.get();
		if(itens){
			this.itens = itens;
			this.itensDisponiveis = this.itens.filter(this.filterItensDisponiveis)
		}
		else
			this.itens = new Array<Item>();
	}

}
