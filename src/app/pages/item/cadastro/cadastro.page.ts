import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import ItemModel from 'src/app/models/ItemModel';
import { NavController, ToastController, LoadingController } from '@ionic/angular';

import idHelper from '../../../helpers/idHelper';
import Item from 'src/app/entities/Item';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})


export class CadastroPage implements OnInit {

	formularioItem: FormGroup;

	imageBase64: any = null;

	callback: Function;

	loading: any;

	isLoading:boolean;
	  
	constructor(private route: ActivatedRoute, private navCtrl: NavController,public toastController: ToastController,public loadingController: LoadingController) { 
		this.criaFormularioVazio();
	}

	ngOnInit() {
	}

	nomeIsValid = () => this.formularioItem.value.nome.length > 0;

	qtdIsValid = () => this.formularioItem.value.qtd > 0;

	imageIsValid = () => this.imageBase64 != null;

	formIsValid = () => this.imageIsValid() && this.qtdIsValid() && this.nomeIsValid();

	async showError() {
		let msg;
		if(!this.qtdIsValid()){
			msg = `A quantidade deve ser maior que 0`
		}else if(!this.nomeIsValid()){
			msg = "Preencha nome do item";
		}else if(!this.imageIsValid()){
			msg = "Escolha uma imagem";
		}else{
			msg = "Tivemos problemas internos... Tente novamente.";
		}

		const toast = await this.toastController.create({
			message: msg,
			duration: 2000
		});

		toast.present();
	}

	async initLoading() {
		this.loading = await this.loadingController.create({
			message: 'Salvando...'
		});

		this.isLoading = true;
		return await this.loading.present();
	}

	async salvaItem() {
		if(!this.formIsValid()){
			this.showError();
			return;
		}

		this.initLoading();
		
		const item: Item = {
			...this.formularioItem.value,
			imagem: this.imageBase64,
			id: idHelper.generate()
		}

		await ItemModel.save(item);

		setTimeout(() => {
			this.backToList();
			this.loading.dismiss();
		},2000);
	}

	backToList() {
		this.navCtrl.back();
	}

	criaFormularioVazio(){
		this.formularioItem = new FormGroup({
			nome: new FormControl(''),
			qtd: new FormControl(''),
		});
	}

	conImage(input) :void{
		if (input.target.files && input.target.files[0]) {
			var reader = new FileReader();
			reader.onload = (e: any) => {
				this.imageBase64 = e.target.result;
			};
			reader.readAsDataURL(input.target.files[0]);
		}
	}

}
