import { Component } from '@angular/core';
import Item from '../../entities/Item';
import ItemModel from 'src/app/models/ItemModel';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

	itens : Array<Item>;
	itensDisponiveis : Array<Item>;

	public callback: Function;

	constructor(private navigate: NavController) {
		
	}

	ngOnInit(){
		this.callback = this.loadItens.bind(this);
	}

	filterItensDisponiveis = item => item.qtd > 0;

	go(route: string) {
		this.navigate.navigateForward(route);
	}

	ionViewWillEnter() {
		console.log("I'm alive!");
		this.loadItens();
	}

	loadItens(){
		console.log("Carregando Itens...");
		const itens = ItemModel.get();
		if(itens){
			this.itens = itens;
			this.itensDisponiveis = this.itens.filter(this.filterItensDisponiveis)
		}
		else
			this.itens = new Array<Item>();
	}


}
