import { Component, OnInit } from '@angular/core';
import Emprestimo from 'src/app/entities/Emprestimo';
import EmprestimoModel from 'src/app/models/EmprestimoModel';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  public callback: Function;

  emprestimos: Array<Emprestimo>;

  filter: string = "todos";

  constructor() {
    this.loadEmprestimos();
  }

  ngOnInit() {
    this.callback = this.loadEmprestimos.bind(this);
  }

  ionViewWillEnter() {
		this.loadEmprestimos();
	}

  loadEmprestimos() {
    const emprestimos = EmprestimoModel.get();
    
    console.log('Itens logo abaixo...');
    console.log(emprestimos);
		if(emprestimos){
			this.emprestimos = emprestimos;
		}
		else
			this.emprestimos = new Array<Emprestimo>();
  }

  filtrar() {
    switch(this.filter){
      case 'todos':
          this.emprestimos = EmprestimoModel.get();
          break;
      case 'devolvidos':
          this.emprestimos = EmprestimoModel.get().filter(emprestimo => emprestimo.dataDevolucao != null);
          break;
      case 'emprestados':
          this.emprestimos = EmprestimoModel.get().filter(emprestimo => emprestimo.dataDevolucao == null);
          break;
    }
  }



}
