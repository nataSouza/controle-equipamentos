import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import Item from 'src/app/entities/Item';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import ItemModel from 'src/app/models/ItemModel';
import Emprestimo from 'src/app/entities/Emprestimo';
import Usuario from 'src/app/entities/Usuario';
import EmprestimoModel from 'src/app/models/EmprestimoModel';
import { NavController, ToastController, LoadingController } from '@ionic/angular';

import idHelper from '../../../helpers/idHelper';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  formularioEmprestimo: FormGroup;

  saveButtonDisabled: boolean = true;

  item: Item;

  emprestimo: Emprestimo;

  loading: any;

  constructor(private route: ActivatedRoute, private navigate: NavController,public toastController: ToastController,public loadingController: LoadingController) {
    this.route.queryParams.subscribe(params => {
      // console.log(params);
      if (params && params.id) {
        this.item = ItemModel.get(params.id);

        this.criaFormulario();
      }
    });
  }

  criaFormulario(){
    // console.log(this.item);
		this.formularioEmprestimo = new FormGroup({
			nome: new FormControl('', [Validators.required]),
      qtd: new FormControl('', [Validators.required]),
      nomeProduto: new FormControl(this.item.nome),
		});
  }
  
  async showError() {
    let msg;
    if(!this.qtdIsValid()){
      msg = `A quantidade deve ser maior que 0 e menor ou igual a ${this.item.qtd}`
    }else if(!this.nomeIsValid()){
      msg = "Preencha o campo de nome";
    }else{
			msg = "Tivemos problemas internos... Tente novamente.";
    }
    
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  async initLoading() {
    this.loading = await this.loadingController.create({
      message: 'Salvando...',
      duration: 2000
    });
    await this.loading.present();
  }
  async save() {
    if(!this.formIsValid()){
      this.showError();
      return;
    }

    this.initLoading();
    const usuario: Usuario = {
      nome: this.formularioEmprestimo.value.nome,
    };
    
    const item_emprestado: Item = {...this.item, qtd: this.formularioEmprestimo.value.qtd};

    const emprestimo: Emprestimo = {
      id: idHelper.generate(),
      usuario,
      item: item_emprestado,
      dataEmprestimo: new Date(),
      dataDevolucao: null
    }

    
    await EmprestimoModel.save(emprestimo);
    this.item.qtd -= item_emprestado.qtd;
    await ItemModel.save(this.item,this.item.id);

    setTimeout(() => {
			this.backToList();
			this.loading.dismiss();
		},2000);
  }

  backToList() {
    this.navigate.back();
  }

  nomeIsValid = () => this.formularioEmprestimo.value.nome.length > 0;
  qtdIsValid = () => this.formularioEmprestimo.value.qtd > 0 && this.formularioEmprestimo.value.qtd <= this.item.qtd;
  formIsValid = () => this.nomeIsValid() && this.qtdIsValid();

  atualizaStatusForm() {
    
  }

  ngOnInit() {
    
  }

}
