import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      { 
        path: 'item-list', 
        loadChildren: '../item/list/list.module#ListPageModule' 
      },
      { 
        path: 'emprestimo-list', 
        loadChildren: '../emprestimo/list/list.module#ListPageModule' 
      },
    ]
  },
  {
    path: '',
    redirectTo: 'tabs/item-list',
    pathMatch:'full'
  }
  
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
