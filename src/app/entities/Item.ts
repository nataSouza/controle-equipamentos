export default interface Item {
    id: string;
    nome: string;
    imagem: string;
    qtd: number;
}   