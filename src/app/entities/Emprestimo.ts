import Item from './Item';
import Usuario from './Usuario';

export default interface Emprestimo {
    id: string;
    item: Item;
    usuario: Usuario;
    dataEmprestimo: Date;
    dataDevolucao: Date;
}