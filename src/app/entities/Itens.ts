import Item from './Item';

export default interface Itens {
    item: Item;
    qtdDisponivel: number;
}