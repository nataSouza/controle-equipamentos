import { Component, OnInit, Input } from '@angular/core';
import Item from 'src/app/entities/Item';
import ItemModel from 'src/app/models/ItemModel';
import { NavigationExtras, Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'itens',
  templateUrl: './itens.component.html',
  styleUrls: ['./itens.component.scss'],
})
export class ItensComponent implements OnInit {

  @Input() item : Item;
  @Input() index : number;
  @Input() renderItens : Function;

  constructor(private navCtrl: NavController) { }

  ngOnInit() {}

  deleteItem() {
    ItemModel.delete(this.item);

    this.renderItens();
  }

  ionViewWillEnter() {
    this.renderItens();
  }

  shareItem() {
    console.log(this.item);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: this.item.id
      }
    };
    this.navCtrl.navigateRoot(['emprestimo/cadastro'], navigationExtras);
  }

}
