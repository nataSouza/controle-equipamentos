import { Component, OnInit, Input } from '@angular/core';
import dateHelper from '../../helpers/dateHelper';
import Emprestimo from 'src/app/entities/Emprestimo';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import Item from 'src/app/entities/Item';
import ItemModel from 'src/app/models/ItemModel';
import EmprestimoModel from 'src/app/models/EmprestimoModel';

@Component({
  selector: 'emprestimos',
  templateUrl: './emprestimos.component.html',
  styleUrls: ['./emprestimos.component.scss'],
})
export class EmprestimosComponent {

  @Input() emprestimo : Emprestimo;
  @Input() index : number;
  @Input() renderEmprestimos : Function;
  @Input() filter: string;

  constructor(private router: Router,public alertController: AlertController,public toastController: ToastController) {
    
  }

  dataEmprestimo() {
    return dateHelper.formatDate(this.emprestimo.dataEmprestimo);
  }
  getLabelStatus() {
    return this.emprestimo.dataDevolucao ? `Devolvido em ${dateHelper.formatDate(this.emprestimo.dataDevolucao)}` : 'Emprestado';
  }

  devolverItem() {
    this.emprestimo.dataDevolucao = new Date();


    const itemDevolvido: Item = { ...this.emprestimo.item };

    let item: Item = ItemModel.get(itemDevolvido.id);

    if(item){
      item.qtd += itemDevolvido.qtd;
      ItemModel.save(item,item.id);
    }else{
      ItemModel.save(itemDevolvido);
    }

    EmprestimoModel.save(this.emprestimo,this.emprestimo.id);
  }

  async confirmDevolucao() {
    if(this.emprestimo.dataDevolucao){
      this.alertEmprestimoJaDevolvido(this.emprestimo.item);
      return;
    }
    const buttonConfirm = {
      name: 'Sim',
      handler: () => this.devolverItem()
    }
    const alert = await this.alertController.create({
      header: 'Devolução',
      message: `Deseja devolver o item ${this.emprestimo.item.nome} ?`,
      buttons: [
        'Não', 
        {
          text: 'Sim',
          handler: () => this.devolverItem()
        }
      ]
    });

    await alert.present();
  }

  async alertEmprestimoJaDevolvido(item: Item) {
    const toast = await this.toastController.create({
      message: `O Item ${item.nome} ja foi devolvido`,
      duration: 2000
    });
    toast.present();
  }

}
