import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItensComponent } from './itens/itens.component';
import { IonicModule } from '@ionic/angular';
import { EmprestimosComponent } from './emprestimos/emprestimos.component';

@NgModule({
  declarations: [ItensComponent,EmprestimosComponent],
  imports: [
    CommonModule,
    IonicModule,
  ],
  exports: [ItensComponent,EmprestimosComponent]
})
export class ComponentModule { }
