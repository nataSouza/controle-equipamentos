import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // { path: '', redirectTo: 'item/list', pathMatch: 'full' },
  // { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)},
  { path: 'item/cadastro', loadChildren: './pages/item/cadastro/cadastro.module#CadastroPageModule' },
  // { path: 'item/list', loadChildren: './pages/item/list/list.module#ListPageModule' },
  { path: 'emprestimo/cadastro', loadChildren: './pages/emprestimo/cadastro/cadastro.module#CadastroPageModule' },
  // { path: 'emprestimo/list', loadChildren: './pages/emprestimo/cadastro/cadastro.module#CadastroPageModule' },
  { path: '', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
