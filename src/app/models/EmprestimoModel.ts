import Emprestimo from '../entities/Emprestimo';

const storageName = "emprestimos";

export default {
    get: (id?: number) => {
        const emprestimos = localStorage.getItem(storageName);

        if(id && emprestimos){
            return JSON.parse(emprestimos).filter(emprestimo => emprestimo.id == id)[0];
        }

        return emprestimos ? JSON.parse(emprestimos) : [];
    },

    save(emprestimo: Emprestimo, id? :string) {
        let emprestimos = this.get();

        if(id){
            console.log('Devolvendo Item...');
            emprestimos = emprestimos.map(emprestimoOfList => emprestimoOfList.id == id ? emprestimoOfList = emprestimo : emprestimoOfList);

        }else{
            console.log('Criando Novo emprestimo...');
            emprestimos.push(emprestimo);
        }

        console.log(emprestimos);

        this.updateAllList(emprestimos);
    },

    delete(emprestimoToDelete: Emprestimo) {
        const emprestimos = this.get();

        const newListEmprestimos = emprestimos.filter(emprestimo => emprestimo.id != emprestimoToDelete.id);
        
        this.updateAllList(newListEmprestimos);
    },

    updateAllList(emprestimos: Array<Emprestimo>){
        localStorage.setItem(storageName,JSON.stringify(emprestimos));
    }
}