import Item from '../entities/Item';

const storageName = "itens";

export default {
    get: (id?: string) => {
        const itens = localStorage.getItem(storageName);
        if(id && itens){
            const itensArray = JSON.parse(itens);
            
            return JSON.parse(itens).filter(item => item.id == id)[0];
        }
        return itens ? JSON.parse(itens) : [];
    },

    save(item: Item,id?: string) {

        let itens = this.get();

        if(id)
            itens = itens.map(itemOfList => itemOfList.id == id ? itemOfList = item : itemOfList);
        else
            itens.push(item);

        this.updateAllList(itens);
    },

    delete(itemToDelete: Item) {
        const itens = this.get();

        const newListItens = itens.filter(item => item.id != itemToDelete.id);

        this.updateAllList(newListItens);
    },

    updateAllList(itens: Array<Item>){
        localStorage.setItem(storageName,JSON.stringify(itens));
    }
}