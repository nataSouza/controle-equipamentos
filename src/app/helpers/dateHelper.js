

export default {
    formatDate: date => {
        if(!date)
            return null;
        
        const dateObj = new Date(date);
        var day = dateObj.getDate();
        var monthIndex = dateObj.getMonth();
        var year = dateObj.getFullYear();
        
        return `${day}/${monthIndex}/${year}`;
    }
}